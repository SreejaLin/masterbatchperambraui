import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')
const SimpleContainer = () => import('@/containers/SimpleContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')


// BNB MASTERS

const BNBHoldReason = () => import('../views/MasterBatchFIFO/Master/BNB_Hold_Reason')
const BNBCompound = () => import('../views/MasterBatchFIFO/Master/BNB_Compound_Master')
const BNBCompoundType = () => import('../views/MasterBatchFIFO/Master/BNB_CompoundType_Master')
const BNBLocation = () => import('../views/MasterBatchFIFO/Master/BNB_Location_Master')
const BNBMaster = () => import('../views/MasterBatchFIFO/Master/BNB_Master')
const BNBMaterialMapping = () => import('../views/MasterBatchFIFO/Master/BNB_MaterialMapping')
//BNB report
const BNBProductionReport = () => import('../views/MasterBatchFIFO/Reports/BNB_Production_Report')
const BNBConsumptionReport = () => import('../views/MasterBatchFIFO/Reports/BNB_Consumption_Report')
const BNBCurrentstockReport = () => import('../views/MasterBatchFIFO/Reports/BNB_Currentstock_Report')
const BNBOveragedReport = () => import('../views/MasterBatchFIFO/Reports/BNB_Overaged_Report')

//BNB TRANSACTIONS 
const BNBProduction = () => import('../views/MasterBatchFIFO/Transaction/BNB_Production')
const BNBLabApproval = () => import('../views/MasterBatchFIFO/Transaction/BNB_LabApproval')
const BNBTechApproval = () => import('../views/MasterBatchFIFO/Transaction/BNB_TechApproval')
const BNBLabEdit = () => import('../views/MasterBatchFIFO/Transaction/BNB_LabEdit')
const BNBRMSTAGPRINT = () => import('../views/MasterBatchFIFO/Transaction/BNB_RMSTagPrint')
const BNBTAGREPRINTTRUCKER = () => import('../views/MasterBatchFIFO/Transaction/BNB_TruckerReprint')
const BNBTAGPRINTBYSO = () => import('../views/MasterBatchFIFO/Transaction/BNB_TagPrintBySO')
const BNBAGERELAXATION = () => import('../views/MasterBatchFIFO/Transaction/BNB_AgeRelaxation')

// Views - Pages for Login
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')
const SkidRetrieval = () => import('@/views/pages/SkidRetrieval')



Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [

    {
      path: '/',
      redirect: '/pages/Login',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'MasterBatchFIFO',
          name: 'MasterBatchFIFO',
          component: {
            render (c) { return c('router-view') }
          },

          children: [
            {
                name: 'Master',
                path:'Master',
                component: {
                  render (c) { return c('router-view') }
                },
                children: [
                  {
                    name: 'BNBHoldReason',
                    component:BNBHoldReason,
                    path:'BNB_Hold_Reason',
                  },
                  {
                    name: 'BNBCompound',
                    component: BNBCompound,
                    path:'BNB_Compound_Master',
                  },
                  {
                    name: 'BNBCompoundType',
                    component: BNBCompoundType,
                    path:'BNB_CompoundType_Master',
                  },
                  {
                    name: 'BNBLocation',
                    component: BNBLocation,
                    path:'BNB_Location_Master',
                  },
                  {
                    name: 'BNBMaster',
                    component: BNBMaster,
                    path:'BNB_Master',
                  },
                  {
                    name: 'BNBMaterialMapping',
                    component: BNBMaterialMapping,
                    path:'BNB_MaterialMapping',
                  },

                  
                ]
            },
            {
              name: 'Transaction',
              path:'Transaction',
              component: {
                render (c) { return c('router-view') }
              },
             children: [
              {
                name: 'BNBProduction',
                component:BNBProduction,
               path:'BNB_Production',
              },              
              {
                name: 'BNBLabApproval',
                component:BNBLabApproval,
               path:'BNB_LabApproval',
              },
              {
                name: 'BNBTechApproval',
                component:BNBTechApproval,
               path:'BNB_TechApproval',
              },
              {
                name: 'BNBLabEdit',
                component:BNBLabEdit,
               path:'BNB_LabEdit',
              },

             

              {
                name: 'BNBTAGREPRINTBYTRUCKER',
                component:BNBTAGREPRINTTRUCKER,
               path:'BNB_TruckerReprint',
              },

              {
                name: 'BNBRMSTAGPRINT',
                component:BNBRMSTAGPRINT,
               path:'BNB_RMSTagPrint',
              },
              {
                name: 'BNBTAGPRINTBYSO',
                component:BNBTAGPRINTBYSO,
               path:'BNB_TagPrintBySO',
              },
              {
                name: 'BNBAGERELAXATION',
                component:BNBAGERELAXATION,
               path:'BNB_AgeRelaxation',
              },
              ]
          }, 
          {
            name: 'Reports',
            path:'Reports',
            component: {
              render (c) { return c('router-view') }
            },
           children: [
            
            {
              name: 'BNBProductionReport',
              component:BNBProductionReport,
              path:'BNB_Production_Report',
            },
            {
              name: 'BNBConsumptionReport',
              component:BNBConsumptionReport,
              path:'BNB_Consumption_Report',
            },
            
            {
              name: 'BNBCurrentstockReport',
              component:BNBCurrentstockReport,
              path:'BNB_Currentstock_Report',
            },
            {
              name: 'BNBOveragedReport',
              component:BNBOveragedReport,
              path:'BNB_Overaged_Report',
            },
            ]
        },
      ]
            },
         
   ]
  },
  {
    path : '/pages',
    redirect :'/pages/Login',
    component: SimpleContainer,
    children: [
     {
       path :'Login',
       name :'Login',
       component :Login
     } 
    ]
  },
  {
    path : '/pages',
    redirect :'/pages/SkidRetrieval',
    component: SimpleContainer,
    children: [
     {
       path :'SkidRetrieval',
       name :'SkidRetrieval',
       component :SkidRetrieval
     },
    
    ]
  }, 
 ]
}

