export default [
  {
    _name: 'CSidebarNav',
    
    _children: [
        {  
         _name: 'CSidebarNavTitle',
          _children: [''] 
        
        },
        {
      
          _name: 'CSidebarNavDropdown',
          name: 'BANBURY FIFO',
          route: '/MasterBatchFIFO',
          icon: { name: 'cil-layers', 
          class: 'text-white' },
          _children: [
        //     {
        //     _name: 'CSidebarNavDropdown',
        //     name: 'Master',
        //     route: '/MasterBatchFIFO/Master',
        //     icon: 'cilTask',
        //     items: [
        //       {
        //         name: 'Machine',
        //         to: '/MasterBatchFIFO/Master/BNB_Master',
        //         icon:'cil-drop'
        //       },
        //       {
        //         name: 'Compounds',
        //         to: '/MasterBatchFIFO/Master/BNB_Compound_Master',
        //         icon:'cil-drop'
        //       },

        //       {
        //         name: 'Compound Types',
        //         to: '/MasterBatchFIFO/Master/BNB_CompoundType_Master',
        //         icon:'cil-drop'
        //       },
        //       {
        //         name: 'Material Mapping',
        //         to: '/MasterBatchFIFO/Master/BNB_MaterialMapping',
        //         icon:'cil-drop'
        //       },
        //       {
        //         name: 'Location',
        //         to: '/MasterBatchFIFO/Master/BNB_Location_Master',
        //         icon:'cil-drop'
        //       },
        //        {
        //         name: 'Hold Reasons',
        //         to: '/MasterBatchFIFO/Master/BNB_Hold_Reason',
        //         icon:'cil-drop'
        //       },
        //     ]

        // },
        {
          _name: 'CSidebarNavDropdown',
          name: 'Transaction',
          route: '/MasterBatchFIFO/Transaction',
          icon: 'cilTask',
          items: [
            {
              name: 'Production Tag Printing',
              to: '/MasterBatchFIFO/Transaction/BNB_Production',
              icon:'cil-drop'
            },
            // {
            //   name: 'Production Tag Re-Printing',
            //   to: '/MasterBatchFIFO/Transaction/BNB_TruckerReprint',
            //   icon:'cil-drop'
            // },
            {
              name: 'Lab Approval',
              to: '/MasterBatchFIFO/Transaction/BNB_LabApproval',
              icon:'cil-drop'
            },
            {
              name: 'Lab Edit',
              to: '/MasterBatchFIFO/Transaction/BNB_LabEdit',
              icon:'cil-drop'
            },

            {
              name: 'Tech Approval',
              to: '/MasterBatchFIFO/Transaction/BNB_TechApproval',
              icon:'cil-drop'
            },
            {
              name: 'Age Releaxtion By Tech',
              to: '/MasterBatchFIFO/Transaction/BNB_AgeRelaxation',
              icon:'cil-drop'
            },
          
            {
              name: 'RMS Tag Print',
              to: '/MasterBatchFIFO/Transaction/BNB_RMSTagPrint',
              icon:'cil-drop'
            },
            {
              name: 'Tag Print By SO',
              to: '/MasterBatchFIFO/Transaction/BNB_TagPrintBySO',
              icon:'cil-drop'
            },
          ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Reports',
        route: '/MasterBatchFIFO/Master',
        icon: 'cilTask',
        items: [
          {
            name: 'Production Report',
            to: '/MasterBatchFIFO/Reports/BNB_Production_Report',
            icon:'cil-drop'
          },
          // {
          //   name: 'Consumption Report',
          //   to: '/MasterBatchFIFO/Reports/BNB_Consumption_Report',
          //   icon:'cil-drop'
          // },
          {
            name: 'Current Stock Report',
            to: '/MasterBatchFIFO/Reports/BNB_Currentstock_Report',
            icon:'cil-drop'
          },
          {
            name: 'OverAged item Report',
            to: '/MasterBatchFIFO/Reports/BNB_Overaged_Report',
            icon:'cil-drop'
          },
        ]

    }
      ],
        
      },  
 

          ]
         },
        ]
  
